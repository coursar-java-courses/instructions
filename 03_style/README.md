# JaCoCo & Checkstyle

**Важно**: это действие выполняется один раз для каждого проекта.

1\. Добавьте в ваш проект JaCoCo (раздел `<build><plugins>...</plugins></build>`):

```xml

<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>${jacoco-maven-plugin.version}</version>
    <executions>
        <execution>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>jacoco-check</id>
            <goals>
                <goal>check</goal>
            </goals>
            <configuration>
                <includes>
                    <include>**/service/*</include>
                </includes>
                <rules>
                    <rule>
                        <element>CLASS</element>
                        <limits>
                            <limit>
                                <counter>BRANCH</counter>
                                <value>COVEREDRATIO</value>
                                <minimum>100%</minimum>
                            </limit>
                        </limits>
                    </rule>
                </rules>
            </configuration>
        </execution>
        <execution>
            <id>jacoco-report</id>
            <phase>test</phase>
            <goals>
                <goal>report</goal>
            </goals>
            <configuration>
                <includes>
                    <include>**/service/*</include>
                </includes>
            </configuration>
        </execution>
    </executions>
</plugin>
```

В качестве версии используйте:

```xml
<jacoco-maven-plugin.version>0.8.7</jacoco-maven-plugin.version>
```

2\. Донастройте Surefire (раздел `<build><plugins>...</plugins></build>`):

```xml

<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>${maven-surefire-plugin.version}</version>
    <configuration>
        <failIfNoTests>true</failIfNoTests>
    </configuration>
</plugin>
```

3\. Добавьте Checkstyle (раздел `<build><plugins>...</plugins></build>`):

```xml

<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-checkstyle-plugin</artifactId>
    <version>${maven-checkstyle-plugin.version}</version>
    <dependencies>
        <dependency>
            <groupId>com.puppycrawl.tools</groupId>
            <artifactId>checkstyle</artifactId>
            <version>${checkstyle.version></version>
        </dependency>
    </dependencies>
    <configuration>
        <failOnViolation>true</failOnViolation>
        <configLocation>checkstyle.xml</configLocation>
        <encoding>UTF-8</encoding>
        <includeTestSourceDirectory>true</includeTestSourceDirectory>
    </configuration>
    <executions>
        <execution>
            <id>validate</id>
            <phase>validate</phase>
            <goals>
                <goal>check</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

Также не забудьте в `properties` указать версию: `<checkstyle.version>9.2</checkstyle.version>`

4\. Добавьте файл с настройками Checkstyle `checkstyle.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE module PUBLIC
        "-//Checkstyle//DTD Checkstyle Configuration 1.3//EN"
        "https://checkstyle.org/dtds/configuration_1_3.dtd">

<module name="Checker">
    <property name="charset" value="UTF-8"/>
    <module name="TreeWalker">
        <module name="MissingOverride"/>
        <module name="LeftCurly"/>
        <module name="NeedBraces"/>
        <module name="EmptyStatement"/>
        <module name="EqualsHashCode"/>
        <module name="SimplifyBooleanReturn"/>
        <module name="StringLiteralEquality"/>
        <module name="OneStatementPerLine"/>
        <module name="IllegalImport">
            <property name="illegalPkgs" value="sun, org.jetbrains.annotations"/>
        </module>
        <module name="RedundantImport"/>
        <module name="UnusedImports"/>
        <module name="UpperEll"/>
        <module name="ArrayTypeStyle"/>
        <module name="MemberName"/>
        <module name="MethodName"/>
        <module name="PackageName">
            <property name="format" value="^[a-z]+(\.[a-z][a-z0-9]*)*$"/>
        </module>
        <module name="ParameterName"/>
        <module name="IllegalIdentifierName"/>
        <module name="OuterTypeNumber"/>
        <module name="GenericWhitespace"/>
        <module name="MethodParamPad"/>
        <module name="ParenPad"/>
        <module name="TypecastParenPad"/>
        <module name="WhitespaceAfter"/>
    </module>
    <module name="LineLength">
        <property name="max" value="150"/>
    </module>
    <module name="NewlineAtEndOfFile">
        <property name="lineSeparator" value="lf"/>
    </module>
    <module name="FileTabCharacter">
        <property name="eachLine" value="true"/>
    </module>
</module>
```

5\. Удостоверьтесь, что ваш `pom.xml` выглядит следующим образом:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <!-- FIXME: вы должны заменить на своё название проекта -->
    <artifactId>distance-prediction</artifactId>
    <version>1.0.2</version>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- FIXME: вы должны заменить на свой Project ID-->
        <gitlab.project.id>32383609</gitlab.project.id>
        <maven-surefire-plugin.version>2.22.2</maven-surefire-plugin.version>
        <junit-jupiter-engine.version>5.8.2</junit-jupiter-engine.version>
        <jacoco-maven-plugin.version>0.8.7</jacoco-maven-plugin.version>
        <maven-checkstyle-plugin.version>3.1.2</maven-checkstyle-plugin.version>
        <checkstyle.version>9.2</checkstyle.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit-jupiter-engine.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <failIfNoTests>true</failIfNoTests>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-check</id>
                        <goals>
                            <goal>check</goal>
                        </goals>
                        <configuration>
                            <includes>
                                <include>**/service/*</include>
                            </includes>
                            <rules>
                                <rule>
                                    <element>CLASS</element>
                                    <limits>
                                        <limit>
                                            <counter>BRANCH</counter>
                                            <value>COVEREDRATIO</value>
                                            <minimum>100%</minimum>
                                        </limit>
                                    </limits>
                                </rule>
                            </rules>
                        </configuration>
                    </execution>
                    <execution>
                        <id>jacoco-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <includes>
                                <include>**/service/*</include>
                            </includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>${maven-checkstyle-plugin.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>com.puppycrawl.tools</groupId>
                        <artifactId>checkstyle</artifactId>
                        <version>${checkstyle.version}</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <failOnViolation>true</failOnViolation>
                    <configLocation>checkstyle.xml</configLocation>
                    <encoding>UTF-8</encoding>
                    <includeTestSourceDirectory>true</includeTestSourceDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>validate</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </repository>
    </repositories>
    <distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </snapshotRepository>
    </distributionManagement>

</project>
```

6\. Можете сохранить свой проект как шаблон IDEA - `Save Project as Template` и при следующих созданиях проекта уже выбирать его (в самом низу - `User Defined Template`):

![](new-project-template.png)
